package ru.ulstu.is.sbapp.company.service.exception;

public class EmployeeNotFoundException extends RuntimeException {
    public EmployeeNotFoundException(Long id) {
        super(String.format("Employee with id [%s] is not found", id));
    }
}
