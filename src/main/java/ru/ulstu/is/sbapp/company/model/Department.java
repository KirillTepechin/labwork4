package ru.ulstu.is.sbapp.company.model;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Entity
public class Department {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @NotBlank(message = "Name of department can't be null or empty")
    private String name;
    @ManyToMany(mappedBy = "departments", fetch=FetchType.EAGER)
    private List<Employee> employees = new ArrayList<>();

    public Department(String name){
        this.name=name;
    }
    public Department(){

    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    public void addEmployee(Employee employee) {
        if(employees.contains(employee)){
            return;
        }
        employees.add(employee);
        employee.addDepartment(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Department department = (Department) o;
        return Objects.equals(id, department.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Position{" +
                "id=" + id +
                ", firstName='" + name + '\'' +
                ", employees='" +  employees.stream()
                                            .map(String::valueOf)
                                            .collect(Collectors.joining("\n")) + '\'' +
                '}';
    }
}
