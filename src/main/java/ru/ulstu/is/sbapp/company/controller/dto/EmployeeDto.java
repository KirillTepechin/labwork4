package ru.ulstu.is.sbapp.company.controller.dto;

import ru.ulstu.is.sbapp.company.model.Employee;

import java.util.*;

public class EmployeeDto {
    private Long id;
    private String name;
    private PositionDto position;
    private List<EmployeeDepartmentDto> departments;

    public EmployeeDto(){

    }
    public EmployeeDto(Employee employee){
        this.id = employee.getId();
        this.name = String.format("%s %s", employee.getFirstName(), employee.getLastName());
        if(employee.getPosition()!=null){
            this.position=new PositionDto(employee.getPosition());
        }
        this.departments = employee.getDepartments().stream()
                .map(EmployeeDepartmentDto::new)
                .toList();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public PositionDto getPosition(){
        return position;
    }

    public List<EmployeeDepartmentDto> getDepartments() {
        return departments;
    }
}
