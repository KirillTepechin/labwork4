package ru.ulstu.is.sbapp.company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ulstu.is.sbapp.company.model.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {
}
