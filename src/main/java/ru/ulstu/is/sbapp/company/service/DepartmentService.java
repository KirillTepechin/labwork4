package ru.ulstu.is.sbapp.company.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.ulstu.is.sbapp.company.controller.dto.DepartmentDto;
import ru.ulstu.is.sbapp.company.model.Department;
import ru.ulstu.is.sbapp.company.model.Employee;
import ru.ulstu.is.sbapp.company.repository.DepartmentRepository;
import ru.ulstu.is.sbapp.company.repository.EmployeeRepository;
import ru.ulstu.is.sbapp.company.service.exception.DepartmentNotFoundException;
import ru.ulstu.is.sbapp.company.service.exception.EmployeeNotFoundException;
import ru.ulstu.is.sbapp.util.validation.ValidatorUtil;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class DepartmentService {
    private final DepartmentRepository departmentRepository;
    private final EmployeeRepository employeeRepository;
    private final ValidatorUtil validatorUtil;

    public DepartmentService(DepartmentRepository departmentRepository,
                             EmployeeRepository employeeRepository,
                             ValidatorUtil validatorUtil){
        this.departmentRepository=departmentRepository;
        this.employeeRepository=employeeRepository;
        this.validatorUtil=validatorUtil;
    }
    @Transactional
    public Department addDepartment(String name) {
        final Department department = new Department(name);
        validatorUtil.validate(department);
        return departmentRepository.save(department);
    }

    @Transactional(readOnly = true)
    public Department findDepartment(Long id) {
        final Optional<Department> department  = departmentRepository.findById(id);
        return department.orElseThrow(() -> new DepartmentNotFoundException(id));
    }

    @Transactional(readOnly = true)
    public List<Department> findAllDepartments() {
        return departmentRepository.findAll();
    }

    @Transactional
    public Department updateDepartment(Long id, String name, List<Employee> employees) {
        final Department currentDepartment = findDepartment(id);
        currentDepartment.setName(name);
        for (Employee employee: employees) {
            currentDepartment.addEmployee(employee);
        }
        validatorUtil.validate(currentDepartment);
        return departmentRepository.save(currentDepartment);
    }

    @Transactional
    public DepartmentDto updateDepartment(DepartmentDto departmentDto) {
        List <Employee> employees = Collections.emptyList();
        String name;
        if(departmentDto.getEmployees()!=null){
            employees = departmentDto.getEmployees().stream()
                    .map(e -> employeeRepository.findById(e.getId())
                            .orElseThrow(()-> new EmployeeNotFoundException(e.getId()))).toList();
        }
        if(departmentDto.getName()==null){
            name = findDepartment(departmentDto.getId()).getName();
        }
        else {
            name = departmentDto.getName();
        }
        return new DepartmentDto(updateDepartment(departmentDto.getId(),
                name,
                employees));
    }

    @Transactional
    public Department deleteDepartment(Long id) {
        final Department department = findDepartment(id);
        departmentRepository.delete(department);
        return department;
    }

    @Transactional
    public void deleteAllDepartments() {
        departmentRepository.deleteAll();
    }

}
