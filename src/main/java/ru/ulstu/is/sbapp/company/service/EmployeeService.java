package ru.ulstu.is.sbapp.company.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import ru.ulstu.is.sbapp.company.controller.dto.EmployeeDto;
import ru.ulstu.is.sbapp.company.model.Department;
import ru.ulstu.is.sbapp.company.model.Employee;
import ru.ulstu.is.sbapp.company.model.Position;
import ru.ulstu.is.sbapp.company.repository.DepartmentRepository;
import ru.ulstu.is.sbapp.company.repository.EmployeeRepository;
import ru.ulstu.is.sbapp.company.repository.PositionRepository;
import ru.ulstu.is.sbapp.util.validation.ValidatorUtil;
import ru.ulstu.is.sbapp.company.service.exception.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final DepartmentRepository departmentRepository;
    private final PositionRepository positionRepository;
    private final ValidatorUtil validatorUtil;

    public EmployeeService(EmployeeRepository employeeRepository,
                           DepartmentRepository departmentRepository,
                           PositionRepository positionRepository,
                           ValidatorUtil validatorUtil){
        this.employeeRepository=employeeRepository;
        this.departmentRepository=departmentRepository;
        this.positionRepository=positionRepository;
        this.validatorUtil=validatorUtil;
    }
    @Transactional
    public Employee addEmployee(String firstName, String lastName) {
        final Employee employee = new Employee(firstName, lastName);
        validatorUtil.validate(employee);
        return employeeRepository.save(employee);
    }

    @Transactional(readOnly = true)
    public Employee findEmployee(Long id) {
        final Optional<Employee> employee  = employeeRepository.findById(id);
        return employee.orElseThrow(() -> new EmployeeNotFoundException(id));
    }

    @Transactional(readOnly = true)
    public List<Employee> findAllEmployees() {
        return employeeRepository.findAll();
    }

    @Transactional
    public Employee updateEmployee(Long id, String firstName, String lastName, List<Department> departments, Position position) {
        final Employee currentEmployee = findEmployee(id);
        currentEmployee.setFirstName(firstName);
        currentEmployee.setLastName(lastName);
        for (Department department: departments) {
            currentEmployee.addDepartment(department);
        }
        currentEmployee.setPosition(position);
        validatorUtil.validate(currentEmployee);
        return employeeRepository.save(currentEmployee);
    }
    @Transactional
    public EmployeeDto updateEmployee(EmployeeDto employeeDto) {
        List <Department> departments = Collections.emptyList();
        Position position = null;
        String firstName;
        String lastName;
        if(employeeDto.getDepartments()!=null){
            departments = employeeDto.getDepartments().stream()
                    .map(dep -> departmentRepository.findById(dep.getId())
                            .orElseThrow(()-> new DepartmentNotFoundException(dep.getId()))).toList();
        }
        if(employeeDto.getPosition()!=null){
            position = positionRepository.findById(employeeDto.getPosition().getId())
                    .orElseThrow(()-> new PositionNotFoundException(employeeDto.getPosition().getId()));
        }
        if(employeeDto.getName() == null){
            firstName = findEmployee(employeeDto.getId()).getFirstName();
            lastName = findEmployee(employeeDto.getId()).getLastName();
        }
        else {
            firstName = employeeDto.getName().split(" ")[0];
            lastName = employeeDto.getName().split(" ")[1];
        }
        return new EmployeeDto(updateEmployee(employeeDto.getId(),
                firstName,
                lastName,
                departments,
                position));
    }

    @Transactional
    public Employee deleteEmployee(Long id) {
        final Employee currentEmployee = findEmployee(id);
        employeeRepository.delete(currentEmployee);
        return currentEmployee;
    }

    @Transactional
    public void deleteAllEmployees() {
        employeeRepository.deleteAll();
    }

}
