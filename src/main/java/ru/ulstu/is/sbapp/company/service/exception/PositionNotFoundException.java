package ru.ulstu.is.sbapp.company.service.exception;

public class PositionNotFoundException extends RuntimeException{
    public PositionNotFoundException(Long id) {
        super(String.format("Position with id [%s] is not found", id));
    }
}
