package ru.ulstu.is.sbapp.company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ulstu.is.sbapp.company.model.Department;

public interface DepartmentRepository extends JpaRepository<Department, Long> {
}
