package ru.ulstu.is.sbapp.company.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.ulstu.is.sbapp.company.controller.dto.DepartmentDto;
import ru.ulstu.is.sbapp.company.controller.dto.PositionDto;
import ru.ulstu.is.sbapp.company.model.Department;
import ru.ulstu.is.sbapp.company.model.Employee;
import ru.ulstu.is.sbapp.company.model.Position;
import ru.ulstu.is.sbapp.company.repository.DepartmentRepository;
import ru.ulstu.is.sbapp.company.repository.EmployeeRepository;
import ru.ulstu.is.sbapp.company.repository.PositionRepository;
import ru.ulstu.is.sbapp.company.service.exception.DepartmentNotFoundException;
import ru.ulstu.is.sbapp.company.service.exception.EmployeeNotFoundException;
import ru.ulstu.is.sbapp.company.service.exception.PositionNotFoundException;
import ru.ulstu.is.sbapp.util.validation.ValidatorUtil;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import java.util.List;
import java.util.Optional;

@Service
public class PositionService {
    private final PositionRepository positionRepository;

    private final ValidatorUtil validatorUtil;

    public PositionService(PositionRepository positionRepository,
                           ValidatorUtil validatorUtil){
        this.positionRepository=positionRepository;
        this.validatorUtil=validatorUtil;
    }
    @Transactional
    public Position addPosition(String name, int salary) {
        final Position position = new Position(name, salary);
        validatorUtil.validate(position);
        return positionRepository.save(position);
    }

    @Transactional(readOnly = true)
    public Position findPosition(Long id) {
        final Optional<Position> position  = positionRepository.findById(id);
        return position.orElseThrow(() -> new PositionNotFoundException(id));
    }

    @Transactional(readOnly = true)
    public List<Position> findAllPositions() {
        return positionRepository.findAll();
    }

    @Transactional
    public Position updatePosition(Long id, String name, int salary) {
        final Position position = findPosition(id);
        position.setName(name);
        position.setSalary(salary);
        validatorUtil.validate(position);
        return positionRepository.save(position);
    }

    @Transactional
    public PositionDto updatePosition(PositionDto positionDto) {
        String name;
        int salary;
        if(positionDto.getName()==null){
            name = findPosition(positionDto.getId()).getName();
        }
        else {
            name = positionDto.getName();
        }
        if(positionDto.getSalary()==0){
            salary = findPosition(positionDto.getId()).getSalary();
        }
        else{
            salary = positionDto.getSalary();
        }
        return new PositionDto(updatePosition(positionDto.getId(),
                name,
                salary));
    }

    @Transactional
    public Position deletePosition(Long id) {
        final Position position = findPosition(id);
        positionRepository.delete(position);
        return position;
    }

    @Transactional
    public void deleteAllPositions() {
        positionRepository.deleteAll();
    }

}
