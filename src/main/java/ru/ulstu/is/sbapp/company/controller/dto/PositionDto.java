package ru.ulstu.is.sbapp.company.controller.dto;

import ru.ulstu.is.sbapp.company.model.Position;


public class PositionDto {
    private Long id;
    private String name;
    private int salary;

    public PositionDto(){

    }
    public PositionDto(Position position){
        this.id=position.getId();
        this.name= position.getName();
        this.salary= position.getSalary();
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getSalary() {
        return salary;
    }
}
