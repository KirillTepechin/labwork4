package ru.ulstu.is.sbapp.student.controller;

import ru.ulstu.is.sbapp.student.model.Student;

public class StudentDto {
    private final long id;
    private final String name;

    public StudentDto(Student student) {
        this.id = student.getId();
        this.name = String.format("%s %s", student.getFirstName(), student.getLastName());
    }

    public long getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
